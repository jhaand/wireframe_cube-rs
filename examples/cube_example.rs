const CUBE_WIRES: [[[i32; 3]; 2]; 12] = [
    [[1, 1, 1], [-1, 1, 1]],
    [[1, -1, 1], [-1, -1, 1]],
    [[1, 1, 1], [1, -1, 1]],
    [[-1, 1, 1], [-1, -1, 1]],
    [[1, 1, -1], [-1, 1, -1]],
    [[1, -1, -1], [-1, -1, -1]],
    [[1, 1, -1], [1, -1, -1]],
    [[-1, 1, -1], [-1, -1, -1]],
    [[1, 1, 1], [1, 1, -1]],
    [[1, -1, 1], [1, -1, -1]],
    [[-1, -1, 1], [-1, -1, -1]],
    [[-1, 1, 1], [-1, 1, -1]],
];

#[derive(Default, Debug)]
struct Position {
    x: f32,
    y: f32,
    z: f32,
}

impl Position {
    fn new(x: f32, y: f32, z: f32) -> Position {
        Position { x: x, y: y, z: z }
    }

    fn rel(&self, dx: f32, dy: f32, dz: f32) -> Position {
        Position {
            x: self.x + dx,
            y: self.y + dy,
            z: self.z + dz,
        }
    }
}

#[derive(Default, Debug)]
struct Wire {
    start: Position,
    end: Position,
}

#[derive(Default, Debug)]
struct Cube {
    wires: [Wire; 12],
}

fn main() {
    let mut cube = Cube::default();

    let pos = Position::new(0.0, 0.0, 0.0);
    let size = 1.0;
    let mut index = 0;

    for wire_tmp in CUBE_WIRES {
        let b_x = wire_tmp[0][0] as f32 * size * 0.5;
        let b_y = wire_tmp[0][1] as f32 * size * 0.5;
        let b_z = wire_tmp[0][2] as f32 * size * 0.5;
        let e_x = wire_tmp[1][0] as f32 * size * 0.5;
        let e_y = wire_tmp[1][1] as f32 * size * 0.5;
        let e_z = wire_tmp[1][2] as f32 * size * 0.5;
        cube.wires[index] = Wire {
            start: pos.rel(b_x, b_y, b_z),
            end: pos.rel(e_x, e_y, e_z),
        };
        index += 1;
    }

    for wire in cube.wires {
        println!("{:?}", wire);
    }
}
