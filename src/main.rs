use nannou::prelude::*;

mod cube;
mod position;
mod wire;

use cube::*;
use position::*;

const SPEED: f64 = 3.0;
const FOV: f32 = PI / 2.0;
const SCREENWIDTH: u32 = 640;
const SCREENHEIGHT: u32 = 480;

fn main() {
    nannou::app(model).update(update).run();
}

struct Model {
    _window: window::Id,
    camera_position: Position,
    direction: f32,
    rotation_y: f32,
    // rotation: Vec2,
    cube: Cube,
}

fn update(app: &App, model: &mut Model, update: Update) {
    // Read Keyboard input in the same manner as
    // ./nannou/wgpu_teapot_camera/wgpu_teapot_camera.rs
    let step_size = (update.since_last.secs() * SPEED) as f32;
    // Go forwards on W
    if app.keys.down.contains(&Key::W) {
        model.camera_position.move_pos(
            step_size * model.direction.cos(),
            step_size * model.direction.sin(),
            0.0,
        );
    }
    // Go backwards on S
    if app.keys.down.contains(&Key::S) {
        model.camera_position.move_pos(
            -step_size * model.direction.cos(),
            -step_size * model.direction.sin(),
            0.0,
        );
    }
    // strafe left on A
    if app.keys.down.contains(&Key::A) {
        model.camera_position.move_pos(
            -step_size * model.direction.sin(),
            step_size * model.direction.cos(),
            0.0,
        );
    }
    // Strafe right on D
    if app.keys.down.contains(&Key::D) {
        model.camera_position.move_pos(
            step_size * model.direction.sin(),
            -step_size * model.direction.cos(),
            0.0,
        );
    }
    
    const ROT_SPEED: f64 = 1.0;
    const ROT_Y_SPEED: f64 = 1.0;
    let rot_step = (update.since_last.secs() * ROT_SPEED) as f32;
    let rot_y_step = (update.since_last.secs() * ROT_Y_SPEED) as f32;
    // Rotate left on H
    if app.keys.down.contains(&Key::H) {
        model.direction += rot_step; 
    }
    if app.keys.down.contains(&Key::L) {
        model.direction -= rot_step; 
    }
    // Look up on K
    if app.keys.down.contains(&Key::K) {
        model.rotation_y += rot_y_step;
    }
    // Look down on J
    if app.keys.down.contains(&Key::J) {
        model.rotation_y -= rot_y_step;
    }
}

fn model(app: &App) -> Model {
    let _window = app
        .new_window()
        .size(SCREENWIDTH, SCREENHEIGHT)
        .view(view)
        .build()
        .unwrap();
    let cube_position = Position::new(6.0, 0.0, 0.0);
    let cube = Cube::new(cube_position, 2.0);
    // for wire in cube.wires {
    //    println!("{:?}", wire);
    //    println!(
    //        "start: {:?}, end: {:?} ",
    //        point_on_canvas(wire.start),
    //        point_on_canvas(wire.end)
    //    );
    // }
    Model {
        _window,
        camera_position: Position::new(0.0, -2.0, 0.0),
        //direction: 0.0,
        direction: PI / 8.0,
        rotation_y: 0.0,
        //rotation: vec2(0.0, 0.0),
        cube,
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    let win = app.main_window().rect();
    // let left_align = win.left() + 20.0;
    let top_align = win.top() - 20.0;

    draw.background().color(BLACK);
    draw.line()
        .start(pt2(-5.0, 0.0))
        .end(pt2(5.0, 0.0))
        .color(STEELBLUE);
    draw.line()
        .start(pt2(0.0, -5.0))
        .end(pt2(0.0, 5.0))
        .color(STEELBLUE);
    for wire in model.cube.wires {
        let cam_pos_start: Position = to_cam_coords(
            wire.start,
            model.camera_position,
            model.direction,
            model.rotation_y,
        );
        let cam_pos_end: Position = to_cam_coords(
            wire.end,
            model.camera_position,
            model.direction,
            model.rotation_y,
        );
        let draw_start: Vec2 = point_on_canvas(cam_pos_start);
        let draw_end: Vec2 = point_on_canvas(cam_pos_end);

        draw.line().start(draw_start).end(draw_end).color(WHITE);
    }
    let status_bar = format!("Camera_positon:\n{:?}\ndirection: {}",model.camera_position, model.direction);
    draw.text(status_bar.as_str())
        .y(top_align)
        .left_justify();
    draw.to_frame(app, &frame).unwrap();
}

fn to_cam_coords(pos: Position, cam: Position, direction: f32, rotation_y: f32) -> Position {
    let mut r_pos: Position = Position::new(pos.x - cam.x, pos.y - cam.y, pos.z - cam.z);

    // calculating rotation
    let mut rx: f32 = r_pos.x;
    let ry: f32 = r_pos.y;

    r_pos.x = rx * (-direction).cos() - ry * (-direction).sin();
    r_pos.y = rx * (-direction).sin() + ry * (-direction).cos();

    rx = r_pos.x;
    let rz: f32 = r_pos.z;

    r_pos.x = rx * (-rotation_y).cos() + rz * (-rotation_y).sin();
    r_pos.z = rz * (-rotation_y).cos() - rx * (-rotation_y).sin();

    r_pos
}

fn point_on_canvas(pos: Position) -> Vec2 {
    let mut angle_h = pos.y.atan2(pos.x);
    let mut angle_v = pos.z.atan2(pos.x);

    // Remove Fishbowl effect
    angle_h /= angle_h.cos().abs();
    angle_v /= angle_v.cos().abs();

    vec2(
        -angle_h * SCREENWIDTH as f32 / FOV,
        -angle_v * SCREENHEIGHT as f32 / FOV,
    )
}
