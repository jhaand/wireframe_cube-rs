use crate::position::*;

#[derive(Default, Debug, Copy, Clone)]
pub struct Wire {
    pub start: Position,
    pub end: Position,
}

#[allow(dead_code)]
impl Wire {
    pub fn new(start: Position, end: Position) -> Wire {
        Wire { start, end }
    }
}
