use crate::position::*;
use crate::wire::*;

#[derive(Default, Debug)]
pub struct Cube {
    pub wires: [Wire; 12],
}

const CUBE_WIRES: [[[i32; 3]; 2]; 12] = [
    [[1, 1, 1], [-1, 1, 1]],
    [[1, -1, 1], [-1, -1, 1]],
    [[1, 1, 1], [1, -1, 1]],
    [[-1, 1, 1], [-1, -1, 1]],
    [[1, 1, -1], [-1, 1, -1]],
    [[1, -1, -1], [-1, -1, -1]],
    [[1, 1, -1], [1, -1, -1]],
    [[-1, 1, -1], [-1, -1, -1]],
    [[1, 1, 1], [1, 1, -1]],
    [[1, -1, 1], [1, -1, -1]],
    [[-1, -1, 1], [-1, -1, -1]],
    [[-1, 1, 1], [-1, 1, -1]],
];

impl Cube {
    pub fn new(pos: Position, size: f32) -> Cube {
        let mut cube = Cube::default();

        for (index, wire_tmp) in CUBE_WIRES.iter().enumerate() {
            let b_x = wire_tmp[0][0] as f32 * size * 0.5;
            let b_y = wire_tmp[0][1] as f32 * size * 0.5;
            let b_z = wire_tmp[0][2] as f32 * size * 0.5;
            let e_x = wire_tmp[1][0] as f32 * size * 0.5;
            let e_y = wire_tmp[1][1] as f32 * size * 0.5;
            let e_z = wire_tmp[1][2] as f32 * size * 0.5;
            cube.wires[index] = Wire {
                start: pos.relative(b_x, b_y, b_z),
                end: pos.relative(e_x, e_y, e_z),
            };
        }
        cube
    }
}
